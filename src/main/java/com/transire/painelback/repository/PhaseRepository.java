package com.transire.painelback.repository;

import com.transire.painelback.models.Phase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PhaseRepository extends JpaRepository<Phase, Integer> {
    List<Phase> findByLineId(Integer lineId);
    Optional<Phase> findByIdAndLineId(Integer id, Integer lineId);
}
