package com.transire.painelback.repository;

import com.transire.painelback.models.Line;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LineRepository extends JpaRepository<Line, Integer> {
    List<Line> findByAreaId(Integer areaId);
    Optional<Line> findByIdAndAreaId(Integer id, Integer areaId);
}
