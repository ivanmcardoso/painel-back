package com.transire.painelback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PainelBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(PainelBackApplication.class, args);
	}
}
