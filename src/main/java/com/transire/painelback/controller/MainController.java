package com.transire.painelback.controller;

import com.transire.painelback.models.Area;
import com.transire.painelback.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/demo", produces = MediaType.APPLICATION_JSON_VALUE)
public class MainController {
    @Autowired
    private AreaRepository areaRepository;

    @GetMapping(path = "/all")
    @ResponseBody
    public List<Area> getAreas(){
        return areaRepository.findAll();
    }

}
