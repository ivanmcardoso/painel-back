package com.transire.painelback.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "PHASE")
public class Phase {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Getter @Setter
    private Integer id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private  String status;
    @Getter @Setter
    private String erro;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "line_id",referencedColumnName = "id")
    private Line line;
}
