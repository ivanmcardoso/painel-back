package com.transire.painelback.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "AREA")
public class Area {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter
    private Integer id;
    @Getter @Setter
    private String name;
    @OneToMany(mappedBy = "area",cascade = CascadeType.ALL)
    @Getter @Setter
    private List<Line> lines;

}
