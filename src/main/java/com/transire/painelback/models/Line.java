package com.transire.painelback.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="LINE")
public class Line {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Getter @Setter
    private Integer id;

    @Getter @Setter
    private Integer lineNumber;

    @OneToMany(mappedBy = "line", cascade = CascadeType.ALL)
    @Getter @Setter
    private List<Phase> phases;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "area_id",referencedColumnName = "id")
    private Area area;
}
