-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: painel
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `phase`
--

DROP TABLE IF EXISTS `phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `erro` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `line_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfclv9jg1jcpcoi9jkxxa5kvru` (`line_id`),
  CONSTRAINT `FKfclv9jg1jcpcoi9jkxxa5kvru` FOREIGN KEY (`line_id`) REFERENCES `line` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase`
--

LOCK TABLES `phase` WRITE;
/*!40000 ALTER TABLE `phase` DISABLE KEYS */;
INSERT INTO `phase` VALUES (1,'','PSN GENERATOR','Ativo',1),(2,'','MESH','Ativo',1),(3,'','MAIN','Ativo',1),(4,'Falha desconhecida','SN MESH','Falha',1),(5,'Etiqueta errada','S0','Falha',2),(6,'','BURNING','Ativo',2),(7,'','S2','Ativo',2),(8,'','QUALIDADE','Ativo',2),(9,'','MAIN','Ativo',4),(10,'','SN MESH','Ativo',4),(11,'','S0','Ativo',5),(12,'','BURNING','Ativo',5),(13,'','S2','Ativo',5),(14,'','QUALIDADE','Ativo',5),(15,'','PSN GENERATOR','Ativo',6),(16,'','MESH','Ativo',6),(17,'','MAIN','Ativo',6),(18,'','SN MESH','Ativo',6),(19,'','PSN GENERATOR','Ativo',7),(22,'','SMT MACHINE','Ativo',7),(25,'','FORNO','Ativo',7),(28,'','INSPEÇÃO','Ativo',7),(31,'','PSN GENERATOR','Ativo',3),(32,'','MESH','Ativo',3),(33,'','MAIN','Ativo',3),(34,'','SN MESH','Ativo',3),(35,'','S0','Ativo',3),(36,'timeout','BURNING','Inativo',3),(37,'','PSN GENERATOR','Ativo',4),(38,'','PSN GENERATOR','Ativo',10),(39,'','MESH','Ativo',10),(40,'','MAIN','Ativo',10),(41,'','SN MESH','Inativo',10),(42,'','S0','Ativo',10),(43,'','BURNING','Ativo',10),(44,'','PSN GENERATOR','Ativo',11),(45,'','PSN GENERATOR','Ativo',12),(46,'','PSN GENERATOR','Ativo',13),(47,'','PSN GENERATOR','Ativo',14),(48,'','PSN GENERATOR','Ativo',15),(49,'','PSN GENERATOR','Ativo',16),(50,'','PSN GENERATOR','Ativo',17),(51,'','PSN GENERATOR','Ativo',18),(52,'','PSN GENERATOR','Ativo',19),(53,'','PSN GENERATOR','Ativo',20),(54,'','PSN GENERATOR','Ativo',21),(55,'','PSN GENERATOR','Ativo',22),(56,'','PSN GENERATOR','Ativo',23),(57,'','PSN GENERATOR','Ativo',24),(58,'','PSN GENERATOR','Ativo',25),(59,'','MESH','Ativo',11),(60,'','MESH','Ativo',12),(61,'','MESH','Ativo',13),(62,'','MESH','Ativo',14),(63,'','MESH','Ativo',15),(64,'','MESH','Ativo',16),(65,'','MESH','Ativo',17),(66,'','MESH','Ativo',18),(67,'','MESH','Ativo',19),(68,'','MESH','Ativo',20),(69,'','MESH','Ativo',21),(70,'','MESH','Ativo',22),(71,'','MESH','Ativo',23),(72,'','MESH','Ativo',24),(73,'','MESH','Ativo',25),(74,'','MAIN','Ativo',11),(75,'','MAIN','Ativo',12),(76,'','MAIN','Ativo',13),(77,'','MAIN','Ativo',14),(78,'','MAIN','Ativo',15),(79,'','MAIN','Ativo',16),(80,'','MAIN','Ativo',17),(81,'','MAIN','Ativo',18),(82,'','MAIN','Ativo',19),(83,'','MAIN','Ativo',20),(84,'','MAIN','Ativo',21),(85,'','MAIN','Ativo',22),(86,'','MAIN','Ativo',23),(87,'','MAIN','Ativo',24),(88,'','MAIN','Ativo',25),(89,'','SN MESH','Ativo',11),(90,'','SN MESH','Ativo',12),(91,'','SN MESH','Ativo',13),(92,'','SN MESH','Ativo',14),(93,'','SN MESH','Ativo',15),(94,'','SN MESH','Ativo',16),(95,'','SN MESH','Ativo',17),(96,'','SN MESH','Ativo',18),(97,'','SN MESH','Ativo',19),(98,'','SN MESH','Ativo',20),(99,'','SN MESH','Ativo',21),(100,'','SN MESH','Ativo',22),(101,'','SN MESH','Ativo',23),(102,'','SN MESH','Ativo',24),(103,'','SN MESH','Ativo',25),(104,'','S0','Ativo',11),(105,'','S0','Ativo',12),(106,'','S0','Ativo',13),(107,'','S0','Ativo',14),(108,'','S0','Ativo',15),(109,'','S0','Ativo',16),(110,'','S0','Ativo',17),(111,'','S0','Ativo',18),(112,'','S0','Ativo',19),(113,'','S0','Ativo',20),(114,'','S0','Ativo',21),(115,'','S0','Ativo',22),(116,'','S0','Ativo',23),(117,'','S0','Ativo',24),(118,'','S0','Ativo',25),(119,'','BURNING','Ativo',11),(120,'','BURNING','Ativo',12),(121,'','BURNING','Ativo',13),(122,'','BURNING','Ativo',14),(123,'','BURNING','Ativo',15),(124,'','BURNING','Ativo',16),(125,'','BURNING','Ativo',17),(126,'','BURNING','Ativo',18),(127,'','BURNING','Ativo',19),(128,'','BURNING','Ativo',20),(129,'','BURNING','Ativo',21),(130,'','BURNING','Ativo',22),(131,'','BURNING','Ativo',23),(132,'','BURNING','Ativo',24),(133,'','BURNING','Ativo',25),(134,'','S2','Ativo',10),(135,'','S2','Ativo',11),(136,'','S2','Ativo',12),(137,'','S2','Ativo',13),(138,'','S2','Ativo',14),(139,'','S2','Ativo',15),(140,'','S2','Ativo',16),(141,'','S2','Ativo',17),(142,'','S2','Ativo',18),(143,'','S2','Ativo',19),(144,'','S2','Ativo',20),(145,'','S2','Ativo',21),(146,'','S2','Ativo',22),(147,'','S2','Ativo',23),(148,'','S2','Ativo',24),(149,'','S2','Ativo',25),(150,'','QUALIDADE','Ativo',10),(151,'','QUALIDADE','Ativo',11),(152,'','QUALIDADE','Ativo',12),(153,'','QUALIDADE','Ativo',13),(154,'','QUALIDADE','Ativo',14),(155,'','QUALIDADE','Ativo',15),(156,'','QUALIDADE','Ativo',16),(157,'','QUALIDADE','Ativo',17),(158,'','QUALIDADE','Ativo',18),(159,'','QUALIDADE','Ativo',19),(160,'','QUALIDADE','Ativo',20),(161,'','QUALIDADE','Ativo',21),(162,'','QUALIDADE','Ativo',22),(163,'','QUALIDADE','Ativo',23),(164,'','QUALIDADE','Ativo',24),(165,'','QUALIDADE','Ativo',25),(166,'','PSN GENERATOR','Ativo',26),(167,'','PSN GENERATOR','Ativo',27),(168,'','PSN GENERATOR','Ativo',28),(169,'','PSN GENERATOR','Ativo',29),(170,'','SMT MACHINE','Ativo',26),(171,'','SMT MACHINE','Ativo',27),(172,'','SMT MACHINE','Ativo',28),(173,'','SMT MACHINE','Ativo',29),(174,'','FORNO','Ativo',26),(175,'','FORNO','Ativo',27),(176,'','FORNO','Ativo',28),(177,'','FORNO','Ativo',29),(178,'','INSPEÇÃO','Ativo',26),(179,'','INSPEÇÃO','Ativo',27),(180,'','INSPEÇÃO','Ativo',28),(181,'','INSPEÇÃO','Ativo',29),(182,'','GRAVAÇÃO','Ativo',26),(183,'','GRAVAÇÃO','Ativo',27),(184,'','GRAVAÇÃO','Ativo',28),(185,'','GRAVAÇÃO','Ativo',29),(186,'','BULK','Ativo',26),(187,'','BULK','Ativo',27),(188,'','BULK','Ativo',28),(189,'','BULK','Ativo',29),(190,'','SHIPMENT','Ativo',26),(191,'','SHIPMENT','Ativo',27),(192,'','SHIPMENT','Ativo',28),(193,'','SHIPMENT','Ativo',29);
/*!40000 ALTER TABLE `phase` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-02 13:47:44
